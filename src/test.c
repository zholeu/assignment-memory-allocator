#include "mem_internals.h"
#include "mem.h"
#include "test.h"
#include "util.h"


static void *block_after(struct block_header const *block) { return (void *) (block->contents + block->capacity.bytes); }
static bool blocks_continuous(struct block_header const *fst, struct block_header const *snd) { return (void *) snd == block_after(fst); }
static struct block_header *get_block_header(void *data) { return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents)); }

char *statusInf[] = {
        [SUCCESS] = "OK",
        [BLOCK_IS_NULL] = "Block is null",
        [BLOCK_LOCATING_ERR] = "While locating the block in a row an error has occurred",
        [BLOCK_IS_FREE_ERR] = "The logic was not right according to task",
};

void write_status(const enum status status, int num) {
    fprintf(stdout,"Test number: %d\nStatus: %s\n",num, statusInf[status]);
}

enum status test_one() {
     struct block_header *first_block = (struct block_header *) heap_init(1000);
    void *block = _malloc(500);
    struct block_header *block_inf = get_block_header(block);
    if (first_block->capacity.bytes != 500) return BLOCK_LOCATING_ERR;
    debug_heap(stdout, block_inf);
    if(block==NULL) {
        _free(block);
        debug_heap(stdout, block_inf);
        return BLOCK_IS_NULL;
    }
    _free(block);
    debug_heap(stdout, block_inf);
    return SUCCESS;
}

enum status test_two() {
     void *block1 = _malloc(100);
    void *block2 = _malloc(100);
    if (block1 == NULL || block2 == NULL) return BLOCK_IS_NULL;
    struct block_header *first_block_data = get_block_header(block1);
    debug_heap(stdout, first_block_data);
    _free(block1);
    struct block_header *second_block_data = get_block_header(block2);
    debug_heap(stdout, second_block_data);
    if (first_block_data->is_free == false || second_block_data->is_free == true) return BLOCK_IS_FREE_ERR;
    _free(block2);
    debug_heap(stdout, first_block_data);
    debug_heap(stdout, second_block_data);
    return SUCCESS;
}

enum status test_three() {
   void *block1 = _malloc(200);
    void *block2 = _malloc(200);
    void *block3 = _malloc(200);
    if (block1 == NULL || block2 == NULL || block3 == NULL) return BLOCK_IS_NULL;
    _free(block1);
    struct block_header *block1_header = get_block_header(block1);
    debug_heap(stdout, block1_header);
    _free(block2);
    struct block_header *block2_header = get_block_header(block2);
    struct block_header *block3_header = get_block_header(block3);
    debug_heap(stdout, block2_header);
    debug_heap(stdout, block3_header);
    if (block1_header->is_free == false || block2_header->is_free == false || block3_header->is_free == true) return BLOCK_IS_FREE_ERR;
    _free(block3);
    debug_heap(stdout, block3_header);
    return SUCCESS;
}

enum status test_four() {
   void *block1 = _malloc(200);
    void *block2 = _malloc(200);
    void *block3 = _malloc(200);
    if (block1 == NULL || block2 == NULL || block3 == NULL) return BLOCK_IS_NULL;
    struct block_header *block1_header = get_block_header(block1);
    struct block_header *block2_header = get_block_header(block2);
    debug_heap(stdout, block1_header);
    debug_heap(stdout, block2_header);
    debug_heap(stdout, block3);
    if (!blocks_continuous(block1_header, block2_header)) return BLOCK_LOCATING_ERR;
    _free(block3);
    _free(block2);
    _free(block1); 
    debug_heap(stdout, block1_header);
    debug_heap(stdout, block2_header);
    debug_heap(stdout, block3);
    return SUCCESS;
}

enum status test_five() {
struct block_header *block_header1 = {0};
    struct block_header *block_header2 = {0};
    struct block_header *block_header3 = {0};
    for (size_t i = 0; i < 200; ++i) {
        void *block1 = _malloc(i);
        void *block2 = _malloc(i+1);
        void *block3 = _malloc(i+2);
        if (block1 == NULL || block2 == NULL || block3 == NULL) return BLOCK_IS_NULL;
        struct block_header *block1_header = get_block_header(block1);
        struct block_header *block2_header = get_block_header(block2);
        struct block_header *block3_header = get_block_header(block3);
        block_header1 = block1_header;
        block_header2 = block1_header;
        block_header3 = block3_header;
        if (!blocks_continuous(block1_header, block2_header)) return BLOCK_LOCATING_ERR;
        _free(block3);
        _free(block2);
        _free(block1);       
     }
    debug_heap(stdout, block_header1);
    debug_heap(stdout, block_header2);
    debug_heap(stdout, block_header3);
    return SUCCESS;
}


